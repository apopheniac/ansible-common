Common
======

A role to install basic tools and admin public keys.

Requirements
------------

Tested on Ansible 1.8 and Ubuntu 14.04 (64 bit)

Role Variables
--------------

# List of admins with public keys to be installed (optional)

common_admins:
  - name: bob
    public_key: "ssh-rsa ohhooyaehoobeep4phahlieGhai4goojae3 bob@bob.com"

Example Playbook
-------------------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: common
	   common_admins:
	     - name: bob
               public_key: "ssh-rsa ohhooyaehoobeep4phahlie bob@bob.com"


License
-------


Author Information
------------------

sam.kitonyi@gmail.com
